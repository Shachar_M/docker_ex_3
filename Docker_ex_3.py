import requests
import sys


def print_html(url_list):
    url_list = url_list.split(',')
    for url in url_list:
        page = requests.get(url)
        print("requesting:", url)
        print("response:", page)
        print(page.content)


if __name__ == "__main__":
    print_html(str(sys.argv[1]))